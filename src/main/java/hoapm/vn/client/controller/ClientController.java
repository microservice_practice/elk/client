package hoapm.vn.client.controller;

import hoapm.vn.client.dto.ProductDTO;
import hoapm.vn.client.proxies.MicroserviceProductProxy;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/client")
public class ClientController {

    private final MicroserviceProductProxy microserviceProductProxy;

    public ClientController(MicroserviceProductProxy microserviceProductProxy) {
        this.microserviceProductProxy = microserviceProductProxy;
    }

    @GetMapping("/product")
    public ResponseEntity<?> getAll() {
        List<ProductDTO> result = this.microserviceProductProxy.findAll();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
