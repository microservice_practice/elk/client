package hoapm.vn.client.proxies;

import hoapm.vn.client.dto.ProductDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(name = "zuul-server", url = "localhost:9004")
public interface MicroserviceProductProxy {
    @GetMapping(value = "/microservice-product/product")
    List<ProductDTO> findAll();

    @GetMapping(value = "/microservice-product/product/{id}")
    ProductDTO findById(@PathVariable("id") Long id);

}
